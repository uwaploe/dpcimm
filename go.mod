module bitbucket.org/uwaploe/dpcimm

go 1.21.5

require (
	bitbucket.org/mfkenney/imm v0.7.2
	bitbucket.org/uwaploe/dpipc v0.1.0
	bitbucket.org/uwaploe/go-dpcmsg/v3 v3.0.0-beta.1
	bitbucket.org/uwaploe/immpkt v0.6.0
	bitbucket.org/uwaploe/tsfpga v1.0.0-beta.3
	github.com/alicebob/miniredis/v2 v2.30.3
	github.com/gomodule/redigo v1.8.8
	github.com/vmihailenco/msgpack/v5 v5.4.1
	go.bug.st/serial v1.6.1
	google.golang.org/grpc v1.44.0
	google.golang.org/protobuf v1.27.1
	gopkg.in/yaml.v2 v2.4.0
	periph.io/x/conn/v3 v3.7.0
	periph.io/x/devices/v3 v3.7.1
	periph.io/x/host/v3 v3.8.0
)

require (
	bitbucket.org/uwaploe/go-mpc v1.3.1 // indirect
	github.com/alicebob/gopher-json v0.0.0-20200520072559-a9ecdc9d1d3a // indirect
	github.com/creack/goselect v0.1.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/snksoft/crc v1.1.0 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	github.com/yuin/gopher-lua v1.1.0 // indirect
	golang.org/x/net v0.0.0-20201021035429-f5854403a974 // indirect
	golang.org/x/sys v0.1.0 // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
