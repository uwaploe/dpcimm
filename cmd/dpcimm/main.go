// This program is part of the Deep Profiler project and manages the
// IMM interface on the DPC
package main

import (
	"context"
	"flag"
	"fmt"
	"net"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"log/slog"

	"bitbucket.org/mfkenney/imm"
	"bitbucket.org/uwaploe/dpcimm/internal/fsm"
	"bitbucket.org/uwaploe/dpcimm/internal/power"
	"github.com/gomodule/redigo/redis"
	"go.bug.st/serial"
	"google.golang.org/grpc"
)

const Usage = `Usage: dpcimm [options] [cfgfile]

Manage the IMM interface on the DPC.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	dumpCfg = flag.Bool("dumpcfg", false,
		"Dump default configuration to stdout and exit")
	doDebug bool
	rpcAddr string = "localhost:10101"
	rdAddr  string = "localhost:6379"
)

type ImmState struct {
	imm    *imm.Device
	pool   *redis.Pool
	conn   redis.Conn
	sw     power.Switch
	err    error
	cfg    *progCfg
	debug  bool
	cmdSvr *net.UDPConn
}

func newPool(server string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		MaxActive:   16,
		IdleTimeout: 120 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprint(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&rpcAddr, "rpc-addr", rpcAddr,
		"host:port for TS-FPGA gRPC server")
	flag.StringVar(&rdAddr, "rd-addr", rdAddr,
		"host:port for Redis server")
	flag.BoolVar(&doDebug, "debug", doDebug,
		"log each received message")

	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpCfg {
		cfg, _ := InitConfig("")
		cfg.Dump(os.Stdout)
		os.Exit(0)
	}

	return flag.Args()
}

func grpcConnect(addr string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())

	return grpc.Dial(addr, opts...)
}

func openSerialPort(cfg portCfg, timeout time.Duration) (serial.Port, error) {
	mode := &serial.Mode{
		BaudRate: cfg.Baud,
		DataBits: 8,
		StopBits: serial.OneStopBit,
	}
	port, err := serial.Open(cfg.Device, mode)
	if err != nil {
		return port, fmt.Errorf("open %s: %w", cfg.Device, err)
	}

	if timeout > 0 {
		_ = port.SetReadTimeout(timeout)
	}

	return port, nil
}

func main() {
	_ = parseCmdLine()

	slog.SetDefault(initLogger())

	cfg, err := InitConfig(flag.Arg(0))
	if err != nil {
		abort("Cannot parse config file", err)
	}

	port, err := openSerialPort(cfg.Port, time.Second*4)
	if err != nil {
		abort("Cannot open serial port", err)
	}

	ms := ImmState{
		imm:   imm.NewDevice(port, imm.ModeIMService),
		cfg:   &cfg,
		pool:  newPool(rdAddr),
		debug: doDebug,
	}

	// Power switch
	gconn, err := grpcConnect(rpcAddr)
	if err != nil {
		abort("Cannot access TS-FPGA server", err)
	}
	ms.sw = power.NewDio(gconn, cfg.Port.Switch)
	// Make sure the power is off
	ms.sw.Off()

	// Create a non-pool Redis connection to log process status.
	conn, err := redis.Dial("tcp", rdAddr)
	if err != nil {
		abort("Cannot access Redis server", err)
	}
	ms.conn = conn

	// Connect to the command server
	addr, err := net.ResolveUDPAddr("udp4", cfg.Server)
	if err != nil {
		abort("address resolve", err, slog.String("addr", cfg.Server))
	}

	ms.cmdSvr, err = net.DialUDP("udp4", nil, addr)
	if err != nil {
		abort("DialUDP", err)
	}

	// Log our process ID
	conn.Do("HSET", "imm", "pid", os.Getpid()) //nolint:errcheck

	// Initialize the FSM
	machine := fsm.New("setup")
	machine.AddState("setup", ms.Setup)
	machine.AddState("receiver", ms.Receiver)
	machine.AddState("error", ms.Err)
	machine.AddState("shutdown", ms.Shutdown)
	machine.AddEndState("done")

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close. If a signal arrives, cancel the Context to
	// cleanly shutdown the FSM.
	go func() {
		s, more := <-sigs
		if more {
			slog.Info("interrupt", slog.String("sig", s.String()))
			cancel()
		}
	}()

	slog.Info("DPC IMM monitor starting", slog.String("version", Version))

	if err := machine.Run(ctx); err != nil {
		slog.Info("exiting", slog.Any("err", err))
	}
}
