package main

import (
	"io"
	"os"

	"gopkg.in/yaml.v2"
)

// Default configuration
var defaultCfg = progCfg{
	Port: portCfg{
		Device: "/dev/ttyAT4",
		Baud:   9600,
		Switch: "8160_LCD_D4",
	},
	Server:     "localhost:4200",
	Warmup:     1000,
	IgnoreTest: true,
	Settings: map[string]interface{}{
		"thost0":                  0,
		"thost1":                  5,
		"thost2":                  1000,
		"thost3":                  6000,
		"thost4":                  100,
		"thost5":                  5,
		"tmodem2":                 3000,
		"tmodem3":                 18000,
		"tmodem4":                 100,
		"enablebinarydata":        1,
		"enableecho":              0,
		"enablehostwakeupcr":      0,
		"enablehostpromptconfirm": 0,
		"enablefullpwrtx":         1,
		"enablebackspace":         0,
		"termtohost":              254,
		"termfromhost":            255,
	},
}

type portCfg struct {
	Device string `yaml:"device"`
	Baud   int    `yaml:"baud"`
	Switch string `yaml:"switch"`
}

type progCfg struct {
	// Serial and power interface description
	Port portCfg `yaml:"port"`
	// Address for command server
	Server string `yaml:"server"`
	// Warm-up time in milliseconds
	Warmup int `yaml:"warmup"`
	// If true, ignore ICC test failures
	IgnoreTest bool `yaml:"ignore_test"`
	// IMM configuration settings
	Settings map[string]interface{} `yaml:"settings"`
}

func InitConfig(path string) (progCfg, error) {
	cfg := defaultCfg
	if path != "" {
		contents, err := os.ReadFile(path)
		if err != nil {
			return cfg, err
		}
		err = cfg.Load(contents)
		if err != nil {
			return cfg, err
		}
	}
	return cfg, nil
}

func (c *progCfg) Load(contents []byte) error {
	return yaml.Unmarshal(contents, c)
}

func (c progCfg) Dump(w io.Writer) error {
	enc := yaml.NewEncoder(w)
	enc.Encode(c)
	return enc.Close()
}
