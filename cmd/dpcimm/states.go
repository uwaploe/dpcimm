// State machine for the IMM communication interface
package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"os"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/uwaploe/immpkt"
)

func (ms *ImmState) Setup(ctx context.Context) string {
	ms.sw.On()

	slog.Info("Power on")
	time.Sleep(time.Duration(ms.cfg.Warmup) * time.Millisecond)
	if err := ms.imm.Wakeup(); err != nil {
		ms.err = fmt.Errorf("No response from IMM: %w", err)
		return "error"
	}

	reply, err := ms.imm.Exec("gethd")
	if err != nil {
		ms.imm.Sleep()
		ms.err = fmt.Errorf("No response from IMM: %w", err)
		return "error"
	}
	slog.Info("IMM ready", slog.String("s/n", reply.Hd.Sn))

	slog.Info("Configuring IMM")
	for k, v := range ms.cfg.Settings {
		slog.Info("IMM parameter change", slog.String("key", k),
			slog.Any("value", v))
		ms.err = ms.imm.Set(k, v)
		if ms.err != nil {
			ms.imm.Sleep()
			slog.Info("Powering down IMM")
			return "error"
		}
	}

	// Run a coupler test
	slog.Info("Running ICC test")
	_, err = ms.imm.Exec("fcl")
	if err != nil {
		ms.err = fmt.Errorf("fcl command error: %w", err)
		return "error"
	}
	reply, err = ms.imm.Exec("t20cc")
	if err != nil && !ms.cfg.IgnoreTest {
		ms.err = fmt.Errorf("t20cc command error: %w", err)
		return "error"
	}

	slog.Info("ICC test complete",
		slog.String("status", reply.Tcc.Status),
		slog.Int("Z", reply.Tcc.Z))

	return "receiver"
}

func readCommand(ctx context.Context, rdr io.Reader) (string, error) {
	b := make([]byte, 1)
	var buf strings.Builder

	for {
		select {
		case <-ctx.Done():
			return buf.String(), ctx.Err()
		default:
		}

		_, err := rdr.Read(b)
		if errors.Is(err, io.EOF) || errors.Is(err, os.ErrDeadlineExceeded) {
			continue
		}
		buf.Write(b)

		if err != nil {
			return buf.String(), err
		}

		if b[0] == '\n' {
			break
		}
	}

	return strings.Trim(buf.String(), " \x00\t\r\n"), nil
}

func (ms *ImmState) Receiver(ctx context.Context) string {
	// Monitor the IMM line for messages from the DSC
	rw, err := ms.imm.Listen()
	if err != nil {
		ms.err = err
		return "error"
	}

	reply := make([]byte, 1024)
	msg := immpkt.Message{}

	for {
		text, err := readCommand(ctx, rw)
		if err != nil {
			ms.err = err
			return "error"
		}

		if len(text) == 0 {
			continue
		}

		// Forward the command to the server and read the reply
		_, err = ms.cmdSvr.Write([]byte(text))
		if err != nil {
			ms.err = err
			return "error"
		}
		ms.cmdSvr.SetReadDeadline(time.Now().Add(time.Second * 5))
		rlen, err := ms.cmdSvr.Read(reply)

		// Send the reply or error back to the IMM link
		t := time.Now()
		if err != nil {
			msg.Class = 'I'
			msg.Payload = []byte(err.Error())
			b, _ := msg.MarshalBinary()
			rw.Write(b)
		} else {
			rw.Write(reply[0:rlen])
		}
		ms.conn.Do("HSET", "imm", "tmsg", strconv.Itoa(int(t.Unix())))

	}

}

func (ms *ImmState) Shutdown(ctx context.Context) string {
	time.Sleep(time.Millisecond * 250)
	ms.sw.Off()
	slog.Info("Power off")
	return "done"
}

func (ms *ImmState) Err(ctx context.Context) string {
	slog.Error("Error state", slog.Any("err", ms.err))
	return "shutdown"
}
