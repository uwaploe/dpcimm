package main

import (
	"encoding/json"
	"testing"
	"time"

	"bitbucket.org/uwaploe/dpipc"
	dpcmsg "bitbucket.org/uwaploe/go-dpcmsg/v3"
	"bitbucket.org/uwaploe/immpkt"
	miniredis "github.com/alicebob/miniredis/v2"
	"github.com/gomodule/redigo/redis"
	"google.golang.org/protobuf/proto"
)

func helperDialRedis(t *testing.T) redis.Conn {
	s := miniredis.RunT(t)
	conn, err := redis.Dial("tcp", s.Addr())
	if err != nil {
		t.Fatal(err)
	}
	return conn
}

func TestStatusMessage(t *testing.T) {
	s := make(map[string]interface{})
	s["pnum"] = int64(42)
	s["mode"] = "stationary"
	s["pressure"] = float64(1234.5)
	s["current"] = int64(321)
	s["vbatt"] = float64(12.1)
	tstamp := time.Now()
	msg := &dpcmsg.Status{}
	err := msg.UnmarshalDataRecord(dpipc.DataRecord{
		Src:  "mmp_1",
		T:    tstamp,
		Data: s})
	if err != nil {
		t.Fatal(err)
	}

	if msg.GetPressure() != 1234500 {
		t.Errorf("Bad pressure value: %v", msg.GetPressure())
	}

	ts := uint64(tstamp.UnixNano()) / 1000
	if msg.GetTime() != ts {
		t.Errorf("Bad timestamp: %v != %v", msg.GetTime(), ts)
	}

	if msg.GetState().String() != "STATIONARY" {
		t.Errorf("Bad state: %v", msg.GetState())
	}

}

func TestSensorMessage(t *testing.T) {
	recs := make([]dpipc.DataRecord, 2)
	s := make(map[string]interface{})
	tstamp1 := time.Now()
	tstamp2 := tstamp1.Add(time.Second)

	s["condwat"] = 33.1
	s["tempwat"] = 8.4
	s["preswat"] = 1234.5
	recs[0] = dpipc.DataRecord{
		Src:  "ctd_1",
		T:    tstamp1,
		Data: s}
	s = make(map[string]interface{})
	s["chlaflo"] = int64(4321)
	s["ntuflo"] = int64(42)
	recs[1] = dpipc.DataRecord{
		Src:  "flntu_1",
		T:    tstamp2,
		Data: s}

	sd := dpcmsg.SensorFromDataRecords(recs)
	if ctds := sd.GetCtd(); ctds[0].GetPreswat() != 1234500 {
		t.Errorf("Bad Ctd pressure value: %v", ctds[0])
	}
	if flntus := sd.GetFlntu(); flntus[0].GetNtuflo() != 42 {
		t.Errorf("Bad Flntu value: %#v", sd.GetFlntu())
	}

}

func TestBatteryMessage(t *testing.T) {
	conn := helperDialRedis(t)
	defer conn.Close()

	m := map[string]string{
		"t":          "123456789",
		"count":      "10",
		"temp":       "5000",
		"voltage":    "15000",
		"current":    "220",
		"energy":     "1200000",
		"rel_charge": "98",
	}

	key := "battery_status"
	_, err := conn.Do("HMSET", redis.Args{}.Add(key).AddFlat(m)...)
	if err != nil {
		t.Fatalf("Cannot store data: %v", err)
	}

	defer conn.Do("DEL", key)

	msg := getBattStatus(conn, key)
	if ts := asInt32(msg["t"]); ts != 123456789 {
		t.Errorf("Record format error; expected 123456789, got %v", ts)
	}

}

func TestAddProfile(t *testing.T) {
	msg := &dpcmsg.Profile{
		Dir:      dpcmsg.Profile_DIVE,
		Queueing: dpcmsg.Profile_REPLACE,
		Depth:    (1234),
		Maxtime:  (12340),
		Requeue:  (0),
		Tstart:   (1800),
		Exclude:  []string{"acs_1", "optode_1"},
	}

	conn := helperDialRedis(t)
	defer conn.Close()

	procProfile(msg, conn)

	qname := "mpc:profiles"
	n, err := redis.Int(conn.Do("LLEN", qname))
	if err != nil {
		t.Fatalf("Redis error: %v", err)
	}
	if n != 1 {
		t.Errorf("Invalid queue size: %v", n)
	}

	reply, err := redis.Bytes(conn.Do("LPOP", qname))
	if err != nil {
		t.Fatalf("Redis error: %v", err)
	}

	pmsg := dpcmsg.ProfileMessage{}
	err = json.Unmarshal(reply, &pmsg)
	if err != nil {
		t.Fatalf("JSON decode error: %v", err)
	}
	if pmsg.Cfg.Deep != 1234 || pmsg.Cfg.StartTime != 1800 {
		t.Errorf("Bad configuration entry: %v", pmsg)
	}

	if pmsg.Exclude[0] != "acs_1" {
		t.Errorf("Exclude list not included: %v", pmsg)
	}

}

func TestProfileMsg(t *testing.T) {
	prof := &dpcmsg.Profile{
		Dir:      dpcmsg.Profile_DIVE,
		Queueing: dpcmsg.Profile_REPLACE,
		Depth:    (1234),
		Maxtime:  (12340),
		Requeue:  (0),
		Tstart:   (1800),
		Exclude:  []string{"acs_1", "optode_1"},
	}

	conn := helperDialRedis(t)
	defer conn.Close()

	b, err := proto.Marshal(prof)
	if err != nil {
		t.Fatal(err)
	}
	msg := immpkt.Message{Class: 'P', Payload: b}

	_, err = procMsg(msg, conn, subSys{})
	if err != nil {
		t.Fatalf("request failed: %v", err)
	}

}
