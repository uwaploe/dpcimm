package main

import (
	"errors"
	"io"
	"os"

	"gopkg.in/yaml.v2"
)

// Default configuration
var defaultCfg progCfg = progCfg{
	RdServer: "localhost:6379",
	TsServer: "localhost:10101",
	Env: envCfg{
		Bus:  "0",
		Addr: 0x77,
	},
	Adc: []adcCfg{
		{Name: "vmon", Cnum: 3, Units: "V", C: []float32{0, 0.001034}},
	},
}

// A/D channel configuration
type adcCfg struct {
	Name     string    `yaml:"name"`
	Cnum     uint      `yaml:"cnum"`
	Units    string    `yaml:"units"`
	UseVolts bool      `yaml:"use_volts"`
	C        []float32 `yaml:"c,flow"`
}

func (a adcCfg) Func() ScaleFunc {
	return polyMult(a.C)
}

// Internal environmental sensor configuration
type envCfg struct {
	Bus  string `yaml:"i2c_bus"`
	Addr int    `yaml:"i2c_addr"`
}

type progCfg struct {
	RdServer string   `yaml:"rd_server"`
	TsServer string   `yaml:"ts_server"`
	Env      envCfg   `yaml:"env"`
	Adc      []adcCfg `yaml:"adc"`
}

type ScaleFunc func(float32) float32

// Return a function that performs the polynomial multiplication
//   y = C[0] + C[1]*x + C[2]*x^2 + ...
func polyMult(c []float32) ScaleFunc {
	return ScaleFunc(func(x float32) float32 {
		y := float32(0)
		for i := len(c) - 1; i > 0; i-- {
			y = x * (c[i] + y)
		}
		return y + c[0]
	})
}

func InitConfig(path string) (progCfg, error) {
	cfg := defaultCfg
	if path != "" {
		contents, err := os.ReadFile(path)
		if err != nil {
			return cfg, err
		}
		err = cfg.Load(contents)
		if err != nil {
			return cfg, err
		}
	}
	return cfg, nil
}

var ErrNotFound = errors.New("A/D channel not found")

func (c progCfg) AdcByName(name string) (adcCfg, error) {
	for _, obj := range c.Adc {
		if obj.Name == name {
			return obj, nil
		}
	}

	return adcCfg{}, ErrNotFound
}

func (c *progCfg) Load(contents []byte) error {
	return yaml.Unmarshal(contents, c)
}

func (c progCfg) Dump(w io.Writer) error {
	enc := yaml.NewEncoder(w)
	enc.Encode(c)
	return enc.Close()
}
