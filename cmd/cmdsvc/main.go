// Cmdsvc provides a UDP server to process DPC commands
//
package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"runtime"

	"log/slog"

	"bitbucket.org/uwaploe/dpcimm/internal/env"
	"bitbucket.org/uwaploe/dpcimm/internal/ips"
	"periph.io/x/conn/v3/i2c/i2creg"
	host "periph.io/x/host/v3"
)

const Usage = `Usage: cmdsvc [options] cfgfile

UDP server to process commands from the dock controller.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	dumpCfg = flag.Bool("dumpcfg", false,
		"Dump default configuration to stdout and exit")
	udpPort int = 4200
	debugEn bool
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprint(os.Stderr, Usage)
		flag.PrintDefaults()
	}
	flag.IntVar(&udpPort, "port", udpPort, "UDP port to listen on")
	flag.BoolVar(&debugEn, "v", debugEn, "Enable verbose debugging output")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpCfg {
		cfg, _ := InitConfig("")
		cfg.Dump(os.Stdout)
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	_ = parseCmdLine()

	slog.SetDefault(initLogger())

	if _, err := host.Init(); err != nil {
		abort("Cannot initialize periph library", err)
	}

	cfg, err := InitConfig(flag.Arg(0))
	if err != nil {
		abort("Cannot parse config file", err)
	}

	// Open I²C bus.
	bus, err := i2creg.Open(cfg.Env.Bus)
	if err != nil {
		abort("failed to open I²C bus", err)
	}
	defer bus.Close()

	sens, err := env.NewSensor(bus, env.I2cAddress(cfg.Env.Addr))
	if err != nil {
		abort("Cannot access BME280", err)
	}

	cln, err := newClient(cfg.TsServer)
	if err != nil {
		abort("Cannot access TS-FPGA server", err, slog.String("addr", cfg.TsServer))
	}
	vsamp, err := initVmonAdc(cfg, cln)
	if err != nil {
		abort("Cannot access vmon A/D", err)
	}

	charger := ips.New(cln)

	conn, err := net.ListenUDP("udp", &net.UDPAddr{
		Port: udpPort,
		IP:   net.ParseIP("0.0.0.0"),
	})
	if err != nil {
		abort("UDP listen", err, slog.Int("port", udpPort))
	}
	defer conn.Close()

	slog.Info("DPC Command Server", slog.String("version", Version),
		slog.String("addr", conn.LocalAddr().String()))
	buf := make([]byte, 1024)

	pool := newPool(cfg.RdServer)
	sys := subSys{charger: charger, vsamp: vsamp, sens: sens, client: cln}

	for {
		rlen, remote, err := conn.ReadFromUDP(buf)
		if err != nil {
			slog.Error("message receive", slog.Any("err", err))
			continue
		}
		msg := string(buf[0:rlen])
		if debugEn {
			slog.Info("message received", slog.Any("source", remote),
				slog.String("req", msg))
		}
		rdconn := pool.Get()
		resp, err := procRequest(msg, rdconn, sys)
		rdconn.Close()
		if err != nil {
			slog.Error("procRequest", slog.Any("err", err), slog.String("req", msg))
			resp.Class = 'E'
			resp.Payload = []byte(err.Error())
		}
		b, _ := resp.MarshalBinary()
		conn.WriteToUDP(b, remote)
	}
}
