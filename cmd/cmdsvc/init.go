package main

import (
	"strings"
	"time"

	"bitbucket.org/uwaploe/dpcimm/internal/adsamp"
	tsadc "bitbucket.org/uwaploe/tsfpga/adc"
	pb "bitbucket.org/uwaploe/tsfpga/api"
	"github.com/gomodule/redigo/redis"
	"google.golang.org/grpc"
)

func newPool(server string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		MaxActive:   16,
		IdleTimeout: 120 * time.Second,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", server)
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func newClient(addr string) (pb.FpgaMemClient, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())
	conn, err := grpc.Dial(addr, opts...)
	if err != nil {
		return nil, err
	}

	return pb.NewFpgaMemClient(conn), nil
}

func initVmonAdc(cfg progCfg, client pb.FpgaMemClient) (*adsamp.Sampler[float32], error) {
	adc, err := tsadc.New(client)
	if err != nil {
		return nil, err
	}
	adc.Configure(tsadc.Size16Bits, tsadc.GainX1, tsadc.AnSelPin77)

	acfg, err := cfg.AdcByName("vmon")
	if err != nil {
		return nil, err
	}

	coeff := acfg.C

	if strings.ToLower(acfg.Units) == "mv" {
		for i := 0; i < len(coeff); i++ {
			coeff[i] = coeff[i] * 0.001
		}
	}
	p, err := adc.PinForChannel(acfg.Cnum - 1)
	if err != nil {
		return nil, err
	}
	s := adsamp.New[float32](p, coeff, acfg.UseVolts)
	return s, nil
}
