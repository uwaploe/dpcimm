// Messaging functions for the DPC communication interface.
package main

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/uwaploe/dpcimm/internal/env"
	"bitbucket.org/uwaploe/dpcimm/internal/ips"
	"bitbucket.org/uwaploe/dpipc"
	dpcmsg "bitbucket.org/uwaploe/go-dpcmsg/v3"
	"bitbucket.org/uwaploe/immpkt"
	pb "bitbucket.org/uwaploe/tsfpga/api"
	"github.com/gomodule/redigo/redis"
	msgpack "github.com/vmihailenco/msgpack/v5"
	"google.golang.org/protobuf/proto"
)

const (
	profQueue  = "mpc:profiles"
	cmdQueue   = "mpc:commands"
	ctlChannel = "motor.control"
)

type EnvSampler interface {
	Sample() (env.DataRecord, error)
}

type AdcSampler interface {
	Sample() float32
}

type subSys struct {
	charger *ips.Ips
	sens    EnvSampler
	vsamp   AdcSampler
	client  pb.FpgaMemClient
}

func dioget(client pb.FpgaMemClient, name string) (pb.DioState, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.DioMsg{Name: name}
	resp, err := client.DioGet(ctx, msg)
	if err != nil {
		return pb.DioState_LOW, err
	}
	return resp.State, nil
}

func pubEnvData(conn redis.Conn, rec env.DataRecord) error {
	const envChan = "data.env"

	b, err := json.Marshal(rec)
	if err != nil {
		return err
	}
	drec := struct {
		T    time.Time       `json:"t"`
		Data json.RawMessage `json:"data"`
	}{T: time.Now(), Data: json.RawMessage(b)}
	b, err = json.Marshal(drec)
	if err != nil {
		return err
	}

	_, err = conn.Do("PUBLISH", envChan, b)

	return err
}

// Add internal environment data to a Status message
func AddEnv(conn redis.Conn, m *dpcmsg.Status, sens EnvSampler) error {
	rec, err := sens.Sample()
	if err != nil {
		return err
	}

	m.Itemp = make([]int32, 0)
	m.Itemp = append(m.Itemp, int32(rec.Temperature*10))
	m.Humidity = int32(rec.Humidity * 10)
	m.Ipr = uint32(rec.Pressure * 10)

	pubEnvData(conn, rec) //nolint:errcheck

	return nil
}

// Return the most recent battery status information from Redis
func getBattStatus(conn redis.Conn, key string) map[string]interface{} {
	var (
		ival int64
		fval float64
	)

	m := make(map[string]interface{})
	result, err := redis.Strings(conn.Do("HGETALL", key))
	if err != nil {
		slog.Error("getBattStatus failed", slog.Any("err", err))
		return m
	}

	// Result is a list of alternating keys and values returned
	// as a list of strings.
	for i := 0; i < len(result); i += 2 {
		k := result[i]
		v := result[i+1]
		ival, err = strconv.ParseInt(v, 0, 64)
		if err == nil {
			m[k] = ival
		} else {
			fval, err = strconv.ParseFloat(v, 64)
			if err == nil {
				m[k] = fval
			} else {
				m[k] = v
			}
		}
	}

	return m
}

func asInt32(v interface{}) int32 {
	switch x := v.(type) {
	case int64:
		return int32(x)
	case float64:
		return int32(x)
	case int32:
		return x
	}

	return 0
}

// Add battery data to a Status message
func AddBattery(m *dpcmsg.Status, conn redis.Conn) {
	key := "battery_status"
	stat := getBattStatus(conn, key)

	// Only include the battery status if it is less than
	// five minutes old.
	if secs, ok := stat["t"].(int64); ok {
		ts := time.Unix(secs, 0)
		if time.Since(ts) > (time.Minute * 5) {
			return
		}
	} else {
		// No time stamp, bogus record
		return
	}

	m.Voltage = asInt32(stat["voltage"])
	m.Current = asInt32(stat["current"])
	m.Energy = asInt32(stat["energy"])
	m.RelCharge = asInt32(stat["rel_charge"])
	m.Itemp = append(m.Itemp, asInt32(stat["temp"]))
}

// Return the most recent data for the named sensor from the
// Redis data-store.
func getSensorData(conn redis.Conn, name string) (dpipc.DataRecord, error) {
	var rec dpipc.DataRecord

	buf, err := redis.Bytes(conn.Do("HGET", "sensors.last", name))
	if err != nil {
		return rec, fmt.Errorf("get sensor %q: %w", name, err)
	}

	// Decode the MessagePack encoded data record
	err = msgpack.Unmarshal(buf, &rec)
	return rec, err
}

// Process a Profile message from the DSC.
func procProfile(p *dpcmsg.Profile, conn redis.Conn) error {

	msg, qing, pat := p.ToProfileMessage()

	// JSON-encode for storage.
	b, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	// Add to the profile queue
	var (
		qcmd  string
		qname string
	)

	// If this profile is part of a pattern, store it in a
	// pattern-specific queue, otherwise, use the default
	// queue.
	if len(pat) > 0 {
		qname = fmt.Sprintf("pattern:%s", strings.ToLower(pat))
	} else {
		qname = profQueue
	}
	switch qing {
	case dpcmsg.Profile_REPLACE:
		conn.Send("DEL", qname)
		slog.Info("Remove profile queue", slog.String("name", qname))
		qcmd = "LPUSH"
	case dpcmsg.Profile_APPEND:
		qcmd = "RPUSH"
	case dpcmsg.Profile_PREPEND:
		qcmd = "LPUSH"
	}
	slog.Info("add profile", slog.String("cmd", qcmd),
		slog.String("contents", string(b)))
	_, err = conn.Do(qcmd, qname, b)

	return err
}

// Schedule a system reboot for some time in the future
func sysReboot(delay time.Duration) {
	go func() {
		time.Sleep(delay)
		slog.Info("Rebooting system")
		cmd := exec.Command("sudo", "reboot")
		cmd.Run()
	}()
}

// Process an incoming packet from the DSC.
func procMsg(msg immpkt.Message, conn redis.Conn, sys subSys) (immpkt.Message, error) {
	resp := immpkt.Message{}
	ok := immpkt.Message{Class: 'I', Payload: []byte("OK")}

	switch msg.Class {
	case 'P':
		slog.Info("New profile received")
		prf := dpcmsg.Profile{}
		if err := proto.Unmarshal(msg.Payload, &prf); err != nil {
			return resp, err
		}
		if err := procProfile(&prf, conn); err != nil {
			return resp, err
		}
		return ok, nil
	case 'P' | 0x80:
		qname := fmt.Sprintf("pattern:%s", bytes.ToLower(msg.Payload))
		exists, err := redis.Int(conn.Do("EXISTS", qname))
		if err != nil {
			return resp, err
		}
		if exists != 1 {
			return resp, fmt.Errorf("Undefined pattern: %q", msg.Payload)
		}

		if _, err = conn.Do("RPUSH", profQueue, qname); err != nil {
			return resp, err
		}
		return ok, nil
	case 'C':
		// Request string encapsulated in a packet
		return procRequest(string(msg.Payload), conn, sys)
	}

	return resp, fmt.Errorf("Invalid message: %s", msg)
}

// Process an incoming request from the DSC. Returns a reply packet and any
// error that occurs
func procRequest(msg string, conn redis.Conn, sys subSys) (immpkt.Message, error) {
	var (
		err  error
		recs []dpipc.DataRecord
	)

	resp := immpkt.Message{}
	okResp := immpkt.Message{Class: 'I', Payload: []byte("OK")}

	fields := strings.Split(msg, " ")
	enc := base64.StdEncoding

	switch fields[0] {
	case "prf":
		slog.Info("New profile received")
		prf := dpcmsg.Profile{}
		if len(fields) < 2 {
			return resp, fmt.Errorf("Missing Profile definition")
		}
		data, err := enc.DecodeString(fields[1])
		if err != nil {
			return resp, err
		}
		if err := proto.Unmarshal(data, &prf); err != nil {
			return resp, err
		}
		if err := procProfile(&prf, conn); err != nil {
			return resp, err
		}
		return okResp, nil
	case "pattern":
		// Add a pattern to the Profiles queue. The pattern (a queue
		// of profile descriptions) must already exist.
		if len(fields) < 2 {
			return resp, fmt.Errorf("Missing pattern name")
		}
		qname := fmt.Sprintf("pattern:%s", strings.ToLower(fields[1]))
		exists, err := redis.Int(conn.Do("EXISTS", qname))
		if err != nil {
			return resp, err
		}
		if exists != 1 {
			return resp, fmt.Errorf("Undefined pattern: %q", fields[1])
		}

		if _, err = conn.Do("RPUSH", profQueue, qname); err != nil {
			return resp, err
		}
		return okResp, nil
	case "dock", "undock", "continue":
		conn.Do("PUBLISH", ctlChannel, fields[0])
		if _, err = conn.Do("RPUSH", cmdQueue, fields[0]); err != nil {
			return resp, err
		}
		return okResp, nil
	case "home", "freewheel", "fw", "brake", "stop":
		// These commands are only supported by the new motor control service.
		n, err := redis.Int(conn.Do("PUBLISH", ctlChannel, fields[0]))
		if err != nil {
			return resp, err
		}
		if n == 0 {
			return resp, errors.New("Message not delivered")
		}
		return okResp, nil
	case "go":
		if n, _ := redis.Int(conn.Do("LLEN", profQueue)); n == 0 {
			return resp, errors.New("Profile queue empty")
		}
		conn.Do("PUBLISH", ctlChannel, fields[0])
		if _, err := conn.Do("RPUSH", cmdQueue, fields[0]); err != nil {
			return resp, err
		}
		return okResp, nil
	case "queue":
		if len(fields) < 2 {
			return resp, fmt.Errorf("Invalid message: %s", msg)
		}
		//nolint:errcheck
		switch fields[1] {
		case "len":
			n, _ := redis.Int(conn.Do("LLEN", profQueue))
			resp.Class = 'I'
			resp.Payload = []byte(strconv.Itoa(n))
			return resp, nil
		case "lpop":
			conn.Do("LPOP", profQueue)
		case "rpop":
			conn.Do("RPOP", profQueue)
		case "del":
			conn.Do("DEL", profQueue)
			conn.Do("DEL", cmdQueue)
		}
		return okResp, nil
	case "ips":
		if len(fields) < 2 {
			return resp, fmt.Errorf("Invalid message: %s", msg)
		}
		switch fields[1] {
		case "en", "enable":
			if err := sys.charger.EnableRectifier(); err != nil {
				return resp, err
			}
		case "dis", "disable":
			if err := sys.charger.DisableRectifier(); err != nil {
				return resp, err
			}
		default:
			return resp, fmt.Errorf("Invalid message: %s", msg)
		}
		return okResp, nil
	case "ping":
		resp.Class = 'I'
		resp.Payload = []byte("pong")
		return resp, nil
	case "reboot":
		var delay time.Duration
		if len(fields) > 1 {
			delay, err = time.ParseDuration(fields[1])
			if err != nil {
				delay = time.Second * 10
			}
		} else {
			delay = time.Second * 10
		}
		sysReboot(delay)
		return okResp, nil
	case "id":
		name, err := os.Hostname()
		if err != nil {
			return resp, err
		}
		parts := strings.Split(name, "-")
		resp.Class = 'I'
		resp.Payload = []byte(parts[len(parts)-1])
		return resp, nil
	case "dioget":
		if len(fields) < 2 {
			return resp, fmt.Errorf("Missing DIO pin name")
		}
		state, err := dioget(sys.client, strings.ToUpper(fields[1]))
		resp.Class = 'I'
		resp.Payload = []byte(state.String())
		return resp, err
	case "sens":
		if len(fields) < 2 {
			return resp, fmt.Errorf("Missing sensor names")
		}

		recs = make([]dpipc.DataRecord, 0)
		for _, name := range fields[1:] {
			rec, err := getSensorData(conn, name)
			if err != nil {
				continue
			}
			recs = append(recs, rec)
		}
		if len(recs) == 0 {
			return resp, fmt.Errorf("No data for %q\n", fields[1:])
		}

		m := dpcmsg.SensorFromDataRecords(recs)
		data, err := proto.Marshal(m)
		if err != nil {
			return resp, err
		}
		resp.Class = 'D'
		resp.Payload = data
		return resp, nil
	case "ev", "event":
		rec, err := getSensorData(conn, "evlogger")
		if err != nil {
			return resp, errors.New("No event log")
		}

		msg, valid := rec.Data.(string)
		if !valid {
			return resp, errors.New("Cannot decode event message")
		}
		resp.Class = 'V'
		resp.Payload = []byte(msg)
		return resp, nil
	case "stat", "status":
		// Get the most recent MPC data record
		rec, err := getSensorData(conn, "mmp_1")
		if err != nil {
			return resp, errors.New("No MMP data available")
		}
		// Unpack into a Status struct
		m := &dpcmsg.Status{}
		if err = m.UnmarshalDataRecord(rec); err != nil {
			return resp, err
		}
		m.UpdateTime(time.Now())
		// Add the voltage monitor A/D data
		if sys.vsamp != nil {
			m.Vmon = int32(sys.vsamp.Sample() * 1000)
		}
		// Add the environmental sensor data
		if sys.sens != nil {
			err := AddEnv(conn, m, sys.sens)
			if err != nil {
				slog.Error("Cannot read env sensor", slog.Any("err", err))
			}
		}
		// Add battery status data
		AddBattery(m, conn)
		// Repackage as a protobuf message
		data, err := proto.Marshal(m)
		if err != nil {
			return resp, err
		}
		resp.Class = 'S'
		resp.Payload = data
		return resp, nil
	default:
		// Check for a base-64 encoded binary packet
		pkt := immpkt.Message{}
		err := pkt.UnmarshalText([]byte(msg))
		if err != nil {
			return resp, fmt.Errorf("Invalid message: %q", msg)
		}
		return procMsg(pkt, conn, sys)
	}

}

//  LocalWords:  nolint
