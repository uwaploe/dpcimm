# DPC IMM Service

This repository contains the software which serves as the command interface for the Deep Profiler vehicle. There are two components, *cmdsvc* and *dpcimm*. Both programs are managed by the [Runit](http://smarden.org/runit/) service supervisor.

## Cmdsvc

This service listens for commands on UDP port 4200, executes them, and returns the response.

## Dpcimm

This services acts as an IMM gateway. It listens for commands over the inductive communication link, passes those commands to *cmdsvc* and passes the responses to the peer.
