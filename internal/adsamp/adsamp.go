// Package adsamp samples an A/D channel.
package adsamp

import (
	"periph.io/x/conn/v3/analog"
	"periph.io/x/conn/v3/physic"
)

type Number interface {
	~float64 | ~float32
}

type ScaleFunc[T Number] func(T) T

// Return a function that performs the polynomial multiplication
//   y = C[0] + C[1]*x + C[2]*x^2 + ...
func polyMult[T Number](c []T) ScaleFunc[T] {
	return ScaleFunc[T](func(x T) T {
		y := T(0)
		for i := len(c) - 1; i > 0; i-- {
			y = x * (c[i] + y)
		}
		return y + c[0]
	})
}

// Sampler samples an analog.PinADC and returns the scaled value
type Sampler[T Number] struct {
	pin      analog.PinADC
	useVolts bool
	fcvt     ScaleFunc[T]
}

func (s *Sampler[T]) Sample() T {
	val, _ := s.pin.Read()
	if s.useVolts {
		return s.fcvt(T(val.V) / T(physic.Volt))
	}
	return s.fcvt(T(val.Raw))
}

// New returns a new Sampler for an ADC pin. Coeff is slice of calibration
// coefficients that are used to convert the sample value to physical
// units. If useVolts is true, the calibration is applied to the ADC input
// voltage value, otherwise it is applied to the raw counts value.
func New[T Number](pin analog.PinADC, coeff []T, useVolts bool) *Sampler[T] {
	return &Sampler[T]{
		pin:      pin,
		useVolts: useVolts,
		fcvt:     polyMult(coeff),
	}
}
