// Package ips controls the Inductive Power System (charging system) on the Deep
// Profiler DPC.
package ips

import (
	"context"
	"time"

	pb "bitbucket.org/uwaploe/tsfpga/api"
)

// Digital outputs for IPS control
const (
	RectifierDisable = "8160_DIO_15"
)

type Ips struct {
	client pb.FpgaMemClient
}

func New(client pb.FpgaMemClient) *Ips {
	return &Ips{
		client: client,
	}
}

func dioset(client pb.FpgaMemClient, name string, state pb.DioState) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.DioMsg{
		Name:  name,
		State: state}
	_, err := client.DioSet(ctx, msg)
	return err
}

func (ips *Ips) EnableRectifier() error {
	return dioset(ips.client, RectifierDisable, pb.DioState_LOW)
}

func (ips *Ips) DisableRectifier() error {
	return dioset(ips.client, RectifierDisable, pb.DioState_HIGH)
}
