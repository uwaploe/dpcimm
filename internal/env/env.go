// Package env provides and interface to the internal environment
// data on the Deep Profiler vehicle controller.
package env

import (
	"periph.io/x/conn/v3/i2c"
	"periph.io/x/conn/v3/physic"
	"periph.io/x/devices/v3/bmxx80"
)

type Sensor struct {
	*bmxx80.Dev
}

type DataRecord struct {
	// Temperature in degrees C
	Temperature float64 `json:"temperature"`
	// Pressure in psi
	Pressure float64 `json:"pressure"`
	// Relative humidity in %
	Humidity float64 `json:"humidity"`
}

func newDataRecord(e physic.Env) DataRecord {
	const paPerPsi float64 = 6894.7573

	rec := DataRecord{}
	rec.Temperature = e.Temperature.Celsius()
	// Convert pressure to psi
	rec.Pressure = float64(e.Pressure) / float64(physic.Pascal) / paPerPsi
	rec.Humidity = float64(e.Humidity) / float64(physic.PercentRH)

	return rec
}

type options struct {
	i2cAddress uint16
}

type Option interface {
	apply(*options)
}

type funcOption struct {
	f func(*options)
}

func (fo *funcOption) apply(opts *options) {
	fo.f(opts)
}

func newFuncOption(f func(*options)) *funcOption {
	return &funcOption{f: f}
}

// Specify the I²C address for the sensor package
func I2cAddress(addr int) Option {
	return newFuncOption(func(opts *options) {
		opts.i2cAddress = uint16(addr)
	})
}

// Enable access the environmental sensor on the specified I²C bus.
func NewSensor(bus i2c.Bus, opts ...Option) (*Sensor, error) {
	var err error
	s := &Sensor{}

	o := options{i2cAddress: 0x76}
	for _, opt := range opts {
		opt.apply(&o)
	}

	s.Dev, err = bmxx80.NewI2C(bus, o.i2cAddress, &bmxx80.Opts{
		Temperature: bmxx80.O1x,
		Pressure:    bmxx80.O1x,
		Humidity:    bmxx80.O1x,
	})

	return s, err
}

// Acquire and return a single data sample
func (s *Sensor) Sample() (DataRecord, error) {
	e := physic.Env{}
	if err := s.Sense(&e); err != nil {
		return DataRecord{}, err
	}

	return newDataRecord(e), nil
}
